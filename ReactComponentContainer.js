class ReactComponentContainer {

    component
    componentProps
    componentContainerId

    constructor(componentContainerId, component, componentProps) {
        if ($('#' + componentContainerId).length == 0) {
            $('body').append('<div id="' + componentContainerId + '"></div>');
        }

        this.componentContainerId = componentContainerId;
        this.component = component;
        this.componentProps = componentProps;
    }

    render(isShow) {
        ReactDOM.render(
            React.createElement(
                antd.ConfigProvider,
                {
                    locale: antd.locales.zh_CN
                },
                React.createElement(this.component, Object.assign({ isShow: isShow }, this.componentProps))
            ),
            document.getElementById(this.componentContainerId)
        );
    }

    show() {
        this.render(true);
    }

    hide() {
        this.render(false);
    }

}

export { ReactComponentContainer }
