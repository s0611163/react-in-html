import { ReactComponentContainer } from './ReactComponentContainer.js'

let isShow = true;
let helloReactContainer;

$('#btn').on('click', function () {
    if (isShow) {
        helloReactContainer = new ReactComponentContainer('helloReact', HelloReact, { name: 'React' });
        helloReactContainer.show();
        isShow = false;
        $(this).val('隐藏React组件');
    } else {
        helloReactContainer.hide();
        isShow = true;
        $(this).val('显示React组件');
    }
});
